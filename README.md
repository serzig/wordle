# Wordle game
This is a demo project of a wordle game implemented in java + swing.
Supported languages are english and russian.

## Examples
<img alt="english variant on linux" src="doc/linux_eng.png" title="English" width="400"/>
<img alt="russian variant on windows" src="doc/windows_rus.png" title="Russian" width="400"/>

## Building and running
Build application manually:
```
./mvnw package
java -jar ./target/wordle-1.0-SNAPSHOT-jar-with-dependencies.jar
```
or download jpackaged binaries from Releases tab.

## Adding new languages
All available languages are in the `languages` directory.
In order to add new language create new folder inside `languages` directory and place two files:
* words.txt - dictionary which will be used for checking entered words and generating hidden words
* config.json - ad-hoc UI internationalization properties, use `languages/English/config.json` for reference

New language should then appear in the Language menu.
