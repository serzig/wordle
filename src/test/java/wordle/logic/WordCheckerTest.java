package wordle.logic;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.List;

public class WordCheckerTest {

    @Test
    public void shouldMissAllLetters() {
        doTest(
                "abcde",
                "fghjk",
                List.of(
                        CheckResult.Missing,
                        CheckResult.Missing,
                        CheckResult.Missing,
                        CheckResult.Missing,
                        CheckResult.Missing
                ));
    }

    @Test
    public void shouldFindOneLetterPreset() {
        doTest(
                "abcde",
                "fghja",
                List.of(
                        CheckResult.Missing,
                        CheckResult.Missing,
                        CheckResult.Missing,
                        CheckResult.Missing,
                        CheckResult.Present
                )
        );
    }

    @Test
    public void shouldFindAllLettersPresent() {
        doTest(
                "абвгд",
                "дгбав",
                List.of(
                        CheckResult.Present,
                        CheckResult.Present,
                        CheckResult.Present,
                        CheckResult.Present,
                        CheckResult.Present
                )
        );
    }

    @Test
    public void shouldFindOneLetterExact() {
        doTest(
                "абвгд",
                "адгвб",
                List.of(
                        CheckResult.ExactPlace,
                        CheckResult.Present,
                        CheckResult.Present,
                        CheckResult.Present,
                        CheckResult.Present
                )
        );
    }

    @Test
    public void shouldFindExcessiveLettersMissing() {
        doTest(
                "аорта",
                "атака",
                List.of(
                        CheckResult.ExactPlace,
                        CheckResult.Present,
                        CheckResult.Missing,
                        CheckResult.Missing,
                        CheckResult.ExactPlace
                )
        );
    }

    @Test
    public void shouldFindRepeatableLettersPresent() {
        doTest(
                "атака",
                "ааота",
                List.of(
                        CheckResult.ExactPlace,
                        CheckResult.Present,
                        CheckResult.Missing,
                        CheckResult.Present,
                        CheckResult.ExactPlace
                )
        );
    }

    @Test
    public void shouldFindRepetableLettersPresent2() {
        doTest(
                "атака",
                "аотaа",
                List.of(
                        CheckResult.ExactPlace,
                        CheckResult.Missing,
                        CheckResult.Present,
                        CheckResult.Missing,
                        CheckResult.ExactPlace
                )
        );
    }

    private void doTest(String word, String guess, List<CheckResult> expected) {
        var checker = new WordChecker(word);
        var actual = checker.check(guess);
        Assertions.assertIterableEquals(expected, actual);
    }
}
