package wordle;

import com.formdev.flatlaf.FlatLightLaf;
import wordle.config.SettingsManager;
import wordle.swing.MainWindow;

public class App {
	public static void main(String[] args) {
		FlatLightLaf.setup();

		var settingsManager = new SettingsManager();
		var mainWindow = new MainWindow(settingsManager);
		mainWindow.startNewGame();
	}
}
