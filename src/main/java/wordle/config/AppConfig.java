package wordle.config;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class AppConfig {
	@SerializedName("alphabet")
	private List<String> alphabet;
	@SerializedName("game")
	private GameConfig game;
	@SerializedName("ui")
	private Ui ui;

	public List<String> getAlphabet() {
		return alphabet;
	}

	public GameConfig getGame() {
		return game;
	}

	public Ui getUi() {
		return ui;
	}

	public static class GameConfig {
		@SerializedName("max_attempts")
		private int maxAttempts;

		public int getMaxAttempts() {
			return maxAttempts;
		}
	}

	public static class Ui {
		@SerializedName("backspace")
		private String backspace;
		@SerializedName("enter_word")
		private String enterWord;
		@SerializedName("exit")
		private String exit;
		@SerializedName("file")
		private String file;
		@SerializedName("lose_text")
		private String loseText;
		@SerializedName("lose_title")
		private String loseTitle;
		@SerializedName("new_game")
		private String newGame;
		@SerializedName("title")
		private String title;
		@SerializedName("win_text")
		private String winText;
		@SerializedName("win_title")
		private String winTitle;

		public String getBackspaceText() {
			return backspace;
		}

		public String getEnterWordText() {
			return enterWord;
		}

		public String getExitText() {
			return exit;
		}

		public String getFileText() {
			return file;
		}

		public String getLoseBodyText() {
			return loseText;
		}

		public String getLoseTitleText() {
			return loseTitle;
		}

		public String getNewGameText() {
			return newGame;
		}

		public String getTitleText() {
			return title;
		}

		public String getWinBodyText() {
			return winText;
		}

		public String getWinTitleText() {
			return winTitle;
		}
	}
}
