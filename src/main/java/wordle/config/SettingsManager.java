package wordle.config;

import com.google.gson.Gson;
import wordle.config.AppConfig;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Properties;
import java.util.stream.Stream;

public class SettingsManager {
	private String language;

	private static final String configFile = "config.xml";

	private List<String> cachedDictionary;
	private AppConfig cachedConfig;

	public SettingsManager() {
		cachedDictionary = null;
		cachedConfig = null;

		var languages = getLanguages();

		try (FileInputStream input = new FileInputStream(configFile)) {
			var properties = new Properties();
			properties.loadFromXML(input);
			language = properties.getProperty("language");
			if (!languages.contains(language)) {
				selectLanguage(languages.get(0));
			}
		} catch (FileNotFoundException e) {
			selectLanguage(languages.get(0));
		} catch (IOException e) {
			// TODO: Implement error handling
			throw new RuntimeException(e);
		}
	}

	public List<String> getLanguages() {
		List<String> languages;

		try (Stream<Path> stream = Files.list(Paths.get("languages"))) {

			languages = stream
					.filter(Files::isDirectory)
					.map(Path::getFileName)
					.map(Path::toString)
					.toList();
		} catch (IOException e) {
			// TODO: Implement error handling
			throw new RuntimeException(e);
		}

		return languages;
	}

	public void selectLanguage(String language) {
		if (this.language != null && this.language.equals(language)) return;

		this.language = language;
		this.cachedConfig = null;
		this.cachedDictionary = null;
		try (OutputStream output = new FileOutputStream(configFile)) {
			var properties = new Properties();
			properties.setProperty("language", this.language);
			properties.storeToXML(output, null);
		} catch (IOException e) {
			// TODO: Implement error handling
			throw new RuntimeException(e);
		}
	}

	public String getLanguage() {
		return this.language;
	}

	public AppConfig getAppConfig() {
		if (cachedConfig != null) {
			return cachedConfig;
		}

		Gson gson = new Gson();
		try {
			cachedConfig = gson.fromJson(Files.newBufferedReader(Paths.get("languages", this.language, "config.json")), AppConfig.class);
			return cachedConfig;
		} catch (IOException e) {
			// TODO: Implement error handling
			throw new RuntimeException(e);
		}
	}

	public List<String> getDictionary() {
		if (cachedDictionary != null) {
			return cachedDictionary;
		}

		try {
			cachedDictionary = Files.readAllLines(Paths.get("languages", this.language, "words.txt"));
			return cachedDictionary;
		} catch (IOException e) {
			// TODO: Implement error handling
			throw new RuntimeException(e);
		}
	}

	public boolean isValidLetter(char letter) {
		String s = Character.toString(letter);
		for (String row : getAppConfig().getAlphabet()) {
			if (row.contains(s)) {
				return true;
			}
		}
		return false;
	}
}
