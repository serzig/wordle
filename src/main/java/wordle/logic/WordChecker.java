package wordle.logic;

import java.util.ArrayList;
import java.util.List;

public class WordChecker {
	private final String word;

	public WordChecker(String word) {
		this.word = word;
	}

	public List<CheckResult> check(String guess) {

		if (word.length() != guess.length()) {
			throw new RuntimeException("Length doesn't match");
		}

		char[] wordChars = word.toCharArray();
		char[] guessChars = guess.toCharArray();
		List<CheckResult> result = new ArrayList<>(guess.length());

		// first, push exact matches
		for (int i = 0; i < guess.length(); i++) {
			char wc = wordChars[i];
			char gc = guessChars[i];

			if (wc == gc) {
				result.add(CheckResult.ExactPlace);
				wordChars[i] = '.';
			} else {
				result.add(null);
			}
		}

		// then, fill in the nulls with present of missing flags
		for (int i = 0; i < result.size(); i++) {
			if (result.get(i) != null) continue;

			CheckResult checkResult = CheckResult.Missing;

			char guessChar = guessChars[i];
			for (int c = 0; c < wordChars.length; c++) {
				if (wordChars[c] == guessChar) {
					checkResult = CheckResult.Present;
					wordChars[c] = '.';
				}
			}

			result.set(i, checkResult);
		}

		return result;
	}
}
