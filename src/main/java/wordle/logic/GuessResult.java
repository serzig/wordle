package wordle.logic;

import java.util.List;

public abstract class GuessResult {

	public static final class GameLost extends GuessResult {
		private final List<CheckResult> checkResult;
		private final String answer;

		public GameLost(List<CheckResult> checkResult, String answer) {
			this.checkResult = checkResult;
			this.answer = answer;
		}

		public String getAnswer() { return this.answer; }

		public List<CheckResult> getCheckResult() {
			return checkResult;
		}
	}

	public static final class GameWon extends GuessResult {
		private final List<CheckResult> checkResult;

		public GameWon(List<CheckResult> checkResult) {
			this.checkResult = checkResult;
		}

		public List<CheckResult> getCheckResult() {
			return checkResult;
		}
	}

	public static final class NextAttempt extends GuessResult {
		private final List<CheckResult> checkResult;

		public NextAttempt(List<CheckResult> checkResult) {
			this.checkResult = checkResult;
		}

		public List<CheckResult> getCheckResult() {
			return checkResult;
		}
	}

	public static final class WordUnknown extends GuessResult { }

	public static final class WordLengthIncorrect extends GuessResult { }
}
