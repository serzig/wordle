package wordle.logic;

public enum CheckResult {
    Missing,
    Present,
    ExactPlace,
    Invalid,
}
