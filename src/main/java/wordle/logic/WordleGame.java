package wordle.logic;

import java.util.List;
import java.util.Objects;
import java.util.Random;

public class WordleGame {
	private final int maxAttempts;
	private final String hiddenWord;
	private final WordChecker wordChecker;
	private final List<String> dictionary;

	private int currentAttempt;
	private boolean isGameOver;

	public WordleGame(int maxAttempts, List<String> dictionary) {
		this.maxAttempts = maxAttempts;
		this.dictionary = Objects.requireNonNull(dictionary);
		this.currentAttempt = 0;
		this.isGameOver = false;

		Random rand = new Random();

		this.hiddenWord = dictionary.get(rand.nextInt(dictionary.size())).toLowerCase();

		this.wordChecker = new WordChecker(this.hiddenWord);
	}

	public int wordLength() {
		return hiddenWord.length();
	}

	public int getCurrentAttempt() {
		return currentAttempt;
	}

	public GuessResult guessWord(String guess) {
		if (isOver()) throw new RuntimeException("Game is over");

		if (guess.length() != hiddenWord.length()) {
			return new GuessResult.WordLengthIncorrect();
		}

		if (!dictionary.contains(guess)) {
			return new GuessResult.WordUnknown();
		}

		List<CheckResult> checkResult = wordChecker.check(guess);
		if (checkResult.stream().allMatch(x -> x == CheckResult.ExactPlace)) {
			this.isGameOver = true;
			return new GuessResult.GameWon(checkResult);
		}

		if (this.currentAttempt + 1 == this.maxAttempts) {
			this.isGameOver = true;
			return new GuessResult.GameLost(checkResult, hiddenWord);
		}

		this.currentAttempt++;

		return new GuessResult.NextAttempt(checkResult);
	}

	public boolean isOver() {
		return isGameOver;
	}
}
