package wordle.swing;

import wordle.config.AppConfig;
import wordle.config.SettingsManager;
import wordle.logic.CheckResult;
import wordle.logic.GuessResult;
import wordle.logic.WordleGame;
import wordle.swing.controls.*;

import javax.swing.*;
import java.awt.*;
import java.util.Objects;

public class MainWindow extends JFrame {

	private final SettingsManager settingsManager;

	private WordleGame game;
	// UI Controls

	private final MainMenuBar mainMenuBar;
	private final GameTitle gameTitle;
	private final GameField gameField;
	private final ControlPanel controlPanel;
	private final Popups popups;

	public MainWindow(SettingsManager settingsManager) {
		this.settingsManager = Objects.requireNonNull(settingsManager);

		var eventListener = new EventListener();

		Styles.init();

		setMinimumSize(new Dimension(400, 550));
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

		JPanel mainPanel = makeMainPanel();
		gameTitle = new GameTitle(mainPanel);
		gameField = new GameField(mainPanel);
		mainPanel.add(Box.createRigidArea(new Dimension(0, 20)));
		controlPanel = new ControlPanel(mainPanel, eventListener);

		mainMenuBar = new MainMenuBar(settingsManager, this, eventListener);
		popups = new Popups(this);

		updateLanguage();

		pack();
		setVisible(true);
	}

	public void startNewGame() {
		int maxAttempts = settingsManager.getAppConfig().getGame().getMaxAttempts();
		game = new WordleGame(maxAttempts, settingsManager.getDictionary());

		gameField.reset(game.wordLength(), maxAttempts);
		controlPanel.resetStyles();
	}

	private JPanel makeMainPanel() {
		var mainPanel = new JPanel();
		var layout = new BoxLayout(mainPanel, BoxLayout.Y_AXIS);
		mainPanel.setLayout(layout);
		mainPanel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
		setContentPane(mainPanel);
		return mainPanel;
	}

	private void updateStyles(int attempt, String word, java.util.List<CheckResult> checkResult) {
		controlPanel.updateStyles(word, checkResult);
		gameField.updateStyles(attempt, checkResult);
	}

	private void updateLanguage() {
		AppConfig appConfig = settingsManager.getAppConfig();

		gameTitle.setText(appConfig.getUi().getTitleText());
		mainMenuBar.updateLanguage(appConfig.getUi());
		controlPanel.recreate(settingsManager);
	}

	// hide interface implementation inside nested class
	class EventListener implements ControlPanel.EventListener, MainMenuBar.EventListener {

		@Override
		public void enterPressedEvent() {
			if (game.isOver()) return;

			int attempt = game.getCurrentAttempt();
			String guess = gameField.getWordAt(attempt);
			GuessResult guessResult = game.guessWord(guess);

			// pattern matching is not available yet
			if (guessResult instanceof GuessResult.GameWon gameWon) {
				updateStyles(attempt, guess, gameWon.getCheckResult());
				popups.showWin(settingsManager.getAppConfig().getUi());

			} else if (guessResult instanceof GuessResult.GameLost gameLost) {
				updateStyles(attempt, guess, gameLost.getCheckResult());
				popups.showLose(settingsManager.getAppConfig().getUi(), gameLost.getAnswer());

			} else if (guessResult instanceof GuessResult.WordUnknown
					|| guessResult instanceof GuessResult.WordLengthIncorrect) {
				gameField.markAttemptInvalid(attempt);

			} else if (guessResult instanceof GuessResult.NextAttempt nextAttempt) {
				updateStyles(attempt, guess, nextAttempt.getCheckResult());
			}
		}

		@Override
		public void backspacePressedEvent() {
			if (game.isOver()) return;

			gameField.deleteChar(game.getCurrentAttempt());
		}

		@Override
		public void letterPressedEvent(char letter) {
			if (game.isOver()) return;
			if (!settingsManager.isValidLetter(letter)) return;

			gameField.pushChar(game.getCurrentAttempt(), letter);
		}

		@Override
		public void newGameEvent() {
			startNewGame();
		}

		@Override
		public void exitEvent() {
			dispose();
		}

		@Override
		public void setLanguageEvent(String language) {
			settingsManager.selectLanguage(language);
			updateLanguage();
			startNewGame();
		}
	}
}
