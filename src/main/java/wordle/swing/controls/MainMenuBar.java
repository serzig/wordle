package wordle.swing.controls;

import wordle.config.AppConfig;
import wordle.config.SettingsManager;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.util.Objects;

public class MainMenuBar {
	private final EventListener listener;

	// UI Controls
	private final JMenu fileMenu;
	private final JMenuItem newGameMenuItem;
	private final JMenuItem exitMenuItem;

	public MainMenuBar(SettingsManager settingsManager, JFrame frame, MainMenuBar.EventListener listener) {
		Objects.requireNonNull(settingsManager);
		Objects.requireNonNull(frame);
		this.listener = Objects.requireNonNull(listener);

		var menuBar = new JMenuBar();

		fileMenu = new JMenu();

		newGameMenuItem = new JMenuItem();
		newGameMenuItem.setAccelerator(KeyStroke.getKeyStroke("ctrl N"));
		newGameMenuItem.addActionListener(this::newGameAction);
		fileMenu.add(newGameMenuItem);

		exitMenuItem = new JMenuItem();
		exitMenuItem.setAccelerator(KeyStroke.getKeyStroke("ctrl Q"));
		exitMenuItem.addActionListener(this::exitAction);

		fileMenu.add(exitMenuItem);

		menuBar.add(fileMenu);

		String selectedLanguage = settingsManager.getLanguage();
		var langMenu = new JMenu("Language");
		var langButtonGroup = new ButtonGroup();
		for (String language : settingsManager.getLanguages()) {
			var langMenuItem = new JRadioButtonMenuItem(language);
			langMenuItem.addActionListener(this::selectLanguageAction);
			if (selectedLanguage.equals(language)) {
				langMenuItem.setSelected(true);
			}
			langButtonGroup.add(langMenuItem);
			langMenu.add(langMenuItem);
		}
		menuBar.add(langMenu);

		frame.setJMenuBar(menuBar);
	}

	public void updateLanguage(AppConfig.Ui uiConfig) {
		fileMenu.setText(uiConfig.getFileText());
		newGameMenuItem.setText(uiConfig.getNewGameText());
		exitMenuItem.setText(uiConfig.getExitText());
	}

	public void newGameAction(ActionEvent event) {
		listener.newGameEvent();
	}

	public void exitAction(ActionEvent event) {
		listener.exitEvent();
	}

	public void selectLanguageAction(ActionEvent event) {
		var language = (JRadioButtonMenuItem) event.getSource();
		language.setSelected(true);
		listener.setLanguageEvent(language.getText());
	}

	public interface EventListener {
		void newGameEvent();
		void exitEvent();
		void setLanguageEvent(String language);
	}
}
