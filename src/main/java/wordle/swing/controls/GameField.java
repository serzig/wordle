package wordle.swing.controls;

import wordle.logic.CheckResult;

import javax.swing.*;
import java.util.List;
import java.util.Objects;

public class GameField {
	private final JPanel panel;

	public GameField(JPanel parent) {
		Objects.requireNonNull(parent);

		panel = new JPanel();
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		parent.add(panel);
	}

	public void reset(int wordLength, int maxAttempts) {
		panel.removeAll();
		for (int i = 1; i <= maxAttempts; i++) {
			var wordPanel = new WordPanel(wordLength);
			panel.add(wordPanel);
		}
	}

	public void pushChar(int attempt, char letter) {
		WordPanel wp = getWordPanelAt(attempt);
		wp.appendChar(letter);
	}

	public void deleteChar(int attempt) {
		WordPanel wp = getWordPanelAt(attempt);
		wp.deleteChar();
	}

	public String getWordAt(int attempt) {
		return getWordPanelAt(attempt).getWord();
	}

	public void updateStyles(int attempt, List<CheckResult> checkResult) {
		WordPanel wp = getWordPanelAt(attempt);
		wp.setCheckResult(checkResult);
	}

	public void markAttemptInvalid(int attempt) {
		WordPanel wp = getWordPanelAt(attempt);
		wp.markWordAsInvalid();
	}

	private WordPanel getWordPanelAt(int attempt) {
		return (WordPanel) panel.getComponent(attempt);
	}
}
