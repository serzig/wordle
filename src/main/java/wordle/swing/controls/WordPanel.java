package wordle.swing.controls;

import wordle.logic.CheckResult;

import javax.swing.*;
import java.awt.*;
import java.util.List;

public class WordPanel extends JPanel {
	private String word;
	private final int maxWordLength;

	public WordPanel(int charCount) {
		word = "";
		maxWordLength = charCount;

		for (int i = 0; i < charCount; i++) {
			add(Box.createRigidArea(new Dimension(i == 0 ? 0 : 5, 0)));
			add(new CharView());
		}

		var layout = new BoxLayout(this, BoxLayout.X_AXIS);
		setLayout(layout);
		setBorder(BorderFactory.createEmptyBorder(2, 0, 3, 0));
		setAlignmentY(Component.TOP_ALIGNMENT);
	}

	public void appendChar(char letter) {
		letter = Character.toLowerCase(letter);

		if (word.length() < maxWordLength) {
			resetStyle();
			setChar(word.length(), letter);
			word += String.valueOf(letter);
		}
	}

	public void deleteChar() {
		if (word.length() > 0) {
			setChar(word.length() - 1, ' ');
			word = word.substring(0, word.length() - 1);
		}
	}

	public void markWordAsInvalid() {
		for (int i = 0; i < word.length(); i++) {
			CharView cv = getCharAt(i);
			cv.setStyle(CheckResult.Invalid);
		}
	}

	public String getWord() {
		return word;
	}

	public void setCheckResult(List<CheckResult> checkResult) {
		for (int i = 0; i < checkResult.size(); i++) {
			CharView cv = getCharAt(i);
			cv.setStyle(checkResult.get(i));
		}
	}

	private void setChar(int position, char letter) {
		CharView cv = getCharAt(position);
		cv.setLetter(letter);
	}

	private int translateIndex(int position) {
		return position * 2 + 1;
	}

	private CharView getCharAt(int position) {
		return (CharView) getComponent(translateIndex(position));
	}

	private void resetStyle() {
		for (int i = 0; i < maxWordLength; i++) {
			CharView cv = getCharAt(i);
			cv.setStyle(null);
		}
	}
}
