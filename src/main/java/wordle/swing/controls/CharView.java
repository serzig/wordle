package wordle.swing.controls;

import wordle.logic.CheckResult;
import wordle.swing.Styles;

import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;

public class CharView extends JPanel {
	private final JLabel label;
	private static final Font font = new Font("sans-serif", Font.PLAIN, 20);

	public CharView() {
		label = new JLabel();
		label.setAlignmentX(CENTER_ALIGNMENT);
		label.setAlignmentY(CENTER_ALIGNMENT);
		label.setFont(font);
		label.setMinimumSize(new Dimension(40, 40));

		label.setText(" ");
		setStyle(null);

		add(label);
	}

	public void setStyle(CheckResult checkResult) {
		if (checkResult == null) {
			Styles.NO_STYLE.applyTo(this);
			Styles.NO_STYLE.applyTo(label);
			setBackground(new Color(251, 252, 255));
			setBorder(new LineBorder(new Color(222, 225, 233), 1,  true));
			label.setForeground(Color.BLACK);
		} else {
			Styles style = Styles.fromCheckResult(checkResult);
			style.applyTo(this);
			style.applyTo(label);
		}
	}

	@Override
	public Dimension getPreferredSize() {
		return new Dimension(40, 40);
	}

	@Override
	public Dimension getMaximumSize() {
		return new Dimension(40, 40);
	}

	public void setLetter(char letter) {
		label.setText(String.valueOf(letter));
	}

}
