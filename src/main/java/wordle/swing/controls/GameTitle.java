package wordle.swing.controls;

import javax.swing.*;
import java.awt.*;
import java.util.Objects;

import static com.formdev.flatlaf.FlatClientProperties.STYLE_CLASS;

public class GameTitle {
	private final JLabel label;

	public GameTitle(JPanel parent) {
		Objects.requireNonNull(parent);

		label = new JLabel();
		label.putClientProperty(STYLE_CLASS, "h1");
		label.setAlignmentX(Component.CENTER_ALIGNMENT);
		label.setBorder(BorderFactory.createEmptyBorder(0, 0, 20, 0));

		parent.add(label);
	}

	public void setText(String text) {
		label.setText(text);
	}
}
