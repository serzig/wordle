package wordle.swing.controls;

import wordle.config.SettingsManager;
import wordle.logic.CheckResult;
import wordle.swing.Styles;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.stream.IntStream;

public class ControlPanel {
	private final EventListener listener;

	// UI Controls
	private final JPanel panel;
	private final Map<String, JButton> keyboard;

	public ControlPanel(JPanel parent, ControlPanel.EventListener listener) {
		Objects.requireNonNull(parent);
		this.listener = Objects.requireNonNull(listener);

		panel = new JPanel();

		// TODO: should use weak refs instead?
		keyboard = new HashMap<>();
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

		parent.add(panel);
	}

	public void recreate(SettingsManager settingsManager) {
		keyboard.clear();
		panel.removeAll();

		var appConfig = settingsManager.getAppConfig();

		// Create characters grid
		for (String row : appConfig.getAlphabet()) {
			char[] chars = row.toCharArray();
			JPanel rowPanel = new JPanel();

			rowPanel.setLayout(new GridLayout(1, chars.length, 5, 0));
			rowPanel.setMaximumSize(new Dimension(30 * chars.length, 30));
			for (char letter : chars) {
				String stringLetter = Character.toString(letter);
				JButton button = new JButton(stringLetter);
				button.addActionListener(this::letterPressedAction);
				rowPanel.add(button);
				keyboard.put(stringLetter, button);
			}
			panel.add(rowPanel);
			panel.add(Box.createRigidArea(new Dimension(0, 5)));
		}

		// create Backspace/Enter control buttons
		var controlPanel = new JPanel();
		controlPanel.setLayout(new BoxLayout(controlPanel, BoxLayout.X_AXIS));

		var backspaceButton = new JButton(appConfig.getUi().getBackspaceText());
		backspaceButton.addActionListener(this::backspacePressedAction);
		controlPanel.add(backspaceButton);

		controlPanel.add(Box.createRigidArea(new Dimension(5, 0)));

		var enterButton = new JButton(appConfig.getUi().getEnterWordText());
		enterButton.addActionListener(this::enterPressedAction);
		controlPanel.add(enterButton);

		panel.add(controlPanel);
	}

	public void resetStyles() {
		for (var button : keyboard.values()) {
			Styles.reset(button);
		}
	}

	public void updateStyles(String word, java.util.List<CheckResult> checkResult) {
		char[] chars = word.toUpperCase().toCharArray();
		var zip = IntStream.range(0, Math.min(chars.length, checkResult.size()))
				.mapToObj(i -> Map.entry(Character.toString(chars[i]), checkResult.get(i)))
				.toList();

		for (var item : zip) {
			var button = keyboard.get(item.getKey());
			Styles newStyle = Styles.fromCheckResult(item.getValue());
			newStyle.applyTo(button);
		}
	}

	public void enterPressedAction(ActionEvent event) {
		listener.enterPressedEvent();
	}

	public void backspacePressedAction(ActionEvent event) {
		listener.backspacePressedEvent();
	}

	public void letterPressedAction(ActionEvent event) {
		char letter = ((JButton) event.getSource()).getText().charAt(0);
		listener.letterPressedEvent(letter);
	}

	public interface EventListener {
		void enterPressedEvent();
		void backspacePressedEvent();
		void letterPressedEvent(char letter);
	}
}
