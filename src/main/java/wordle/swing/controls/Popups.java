package wordle.swing.controls;

import wordle.config.AppConfig;

import javax.swing.*;

import java.awt.*;

import static javax.swing.JOptionPane.ERROR_MESSAGE;
import static javax.swing.JOptionPane.INFORMATION_MESSAGE;

public class Popups {
	private final Component parent;

	public Popups(Component parent) {
		this.parent = parent;
	}

	public void showWin(AppConfig.Ui ui) {
		JOptionPane.showMessageDialog(
				parent, ui.getWinBodyText(), ui.getWinTitleText(), INFORMATION_MESSAGE);
	}

	public void showLose(AppConfig.Ui ui, String answer) {
		JOptionPane.showMessageDialog(
				parent, ui.getLoseBodyText() + answer, ui.getLoseTitleText(), ERROR_MESSAGE);
	}
}
