package wordle.swing;

import wordle.logic.CheckResult;

import javax.swing.*;

import static com.formdev.flatlaf.FlatClientProperties.STYLE_CLASS;

public enum Styles {

	NO_STYLE("nostyle", 0),
	INVALID("invalid", 0),
	MISSING("missing", 100),
	PRESENT("present", 200),
	EXACT_PLACE("exactplace", 300);

	private final String name;
	private final int level;

	Styles(String name, int level) {
		this.name = name;
		this.level = level;
	}

	public static void init() {

		UIManager.put("[style]." + MISSING.name, "background: #A4AEC4; foreground: #fff;");
		UIManager.put("[style]." + PRESENT.name, "background: #f3c237; foreground: #fff;");
		UIManager.put("[style]." + EXACT_PLACE.name, "background: #79b851; foreground: #fff;");
		UIManager.put("[style]." + INVALID.name, "foreground: #BF3C4B;");
	}

	public static Styles fromCheckResult(CheckResult checkResult) {
		return switch (checkResult) {
			case Invalid -> Styles.INVALID;
			case Missing -> Styles.MISSING;
			case Present -> Styles.PRESENT;
			case ExactPlace -> Styles.EXACT_PLACE;
		};
	}

	private static Styles fromString(String input) {
		if (input == null) return NO_STYLE;
		if (NO_STYLE.name.equals(input)) return NO_STYLE;
		if (INVALID.name.equals(input)) return INVALID;
		if (MISSING.name.equals(input)) return MISSING;
		if (PRESENT.name.equals(input)) return PRESENT;
		if (EXACT_PLACE.name.equals(input)) return EXACT_PLACE;

		throw new RuntimeException("Unknown style");
	}

	public static void reset(JComponent component) {
		component.putClientProperty(STYLE_CLASS, Styles.NO_STYLE.name);
	}

	public void applyTo(JComponent component) {
		String oldClass = (String) component.getClientProperty(STYLE_CLASS);

		if (this.level >= fromString(oldClass).level) {
			component.putClientProperty(STYLE_CLASS, name);
		}
	}
}
